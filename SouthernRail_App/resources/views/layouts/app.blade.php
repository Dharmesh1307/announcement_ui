 <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Southern Railways') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style> 

     .navbar-nav li {
        padding-right: 15px;
        
      }

       .navbar-nav li:hover {
        background-color: orange;

      }

      .whitelia{         
        color:white;
      }



      .zIndex2 {
          z-index: 2;

        }
        .cf::after {
          clear: both;
          content: ' ';
          display: block;
          font-size: 0;
          line-height: 0;
          visibility: hidden;
          width: 0;
          height: 0;
      }
      .center {
          width: 1000px;
          position: relative;
          z-index: 1;
          margin: 0 auto;
      }

      #header {
          width: 100%;
          min-width: 1000px;
          padding-top: 10px;
          position: relative;
          z-index: 999;
          background: url(../../../Images/header-bg.gif) left top repeat-x;
        }
        #header {
          z-index: 1001 !important;
      }



      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      /* GLOBAL STYLES
      -------------------------------------------------- */
      /* Padding below the footer and lighter body text */

      body {
        padding-top: 3rem;
        padding-bottom: 3rem;
        color: #5a5a5a;
        font: 1em/1.3em droid_sansregular, Arial, sans-serif;
      }


      /* CUSTOMIZE THE CAROUSEL
      -------------------------------------------------- */

      /* Carousel base class */
      .carousel {
        margin-bottom: 4rem;
      }
      /* Since positioning the image, we need to help out the caption */
      .carousel-caption {
        bottom: 3rem;
        z-index: 10;
      }

      /* Declare heights because of positioning of img element */
      .carousel-item {
        height: 32rem;
      }
      .carousel-item > img {
        position: absolute;
        top: 0;
        left: 0;
        min-width: 100%;
        height: 32rem;
      }


      /* MARKETING CONTENT
      -------------------------------------------------- */

      /* Center align the text within the three columns below the carousel */
      .marketing .col-lg-4 {
        margin-bottom: 1.5rem;
        text-align: center;
      }
      .marketing h2 {
        font-weight: 400;
      }
      .marketing .col-lg-4 p {
        margin-right: .75rem;
        margin-left: .75rem;
      }


      /* Featurettes
      ------------------------- */

      .featurette-divider {
        margin: 5rem 0; /* Space out the Bootstrap <hr> more */
      }

      /* Thin out the marketing headings */
      .featurette-heading {
        font-weight: 300;
        line-height: 1;
        letter-spacing: -.05rem;
      }


      /* RESPONSIVE CSS
      -------------------------------------------------- */

      @media (min-width: 40em) {
        /* Bump up size of carousel content */
        .carousel-caption p {
          margin-bottom: 1.25rem;
          font-size: 1.25rem;
          line-height: 1.4;
        }

        .featurette-heading {
          font-size: 50px;
        }
      }

      @media (min-width: 62em) {
        .featurette-heading {
          margin-top: 7rem;
        }
      }

    </style>

 


  </head>
  <body>
<header>
    <!--<div class="headerTop cf center zIndex2">f5f5f5
      
    <h1 id="logo" class="alignLeft" title="Air India">
          <a title="Air India" href="index.htm" class="logo1 displayNone">
              <img src="Images/new-AI-alliance-logo.png" alt="Air India" title="Air India" width="229" height="69"/>
          </a>
          <a title="Air India" href="index.htm" class="logo1">
              <img src="Images/logo-new.png" alt="Air India" title="Air India" width="208" height="69"/>
          </a>
          <a href="http://www.staralliance.com" target="_blank" class="logo2">
              <img title="Star Alliance" alt="Star Alliance" src="Images/logo-new-star-alliance.png" width="253" height="69"/>
          </a>
      </h1>

    </div>-->

  <nav class=" shift navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #c32026;">
    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('/images/New_Logo.jpg') }}" class="logo" style="height: 50px;" />  
        <!--{{ config('app.name', 'Southern Railways') }} -->
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <!-- Authentication Links -->
        @if (Route::has('login'))
            <li><a class="nav-link" href="{{ route('trains.index') }}"  style="color:white;">  Trains </a> </li>
           
            <li><a class="nav-link" href="#" style="color:white;">Enquiry</a></li>
              <!-- @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </li>
                  
              @endguest -->
        @endif
      </ul>
      <!-- <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> -->
       @guest
          <ul class="navbar-nav mr-auto" style="list-style-type:none;">
              <li>
                  <a class="nav-link fa fa-sign-in-alt" href="{{ route('login') }}"  style="color:white;">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
                  <li>
                      <a class="nav-link" href="{{ route('register') }}"  style="color:white;">{{ __('Register') }}</a>
                  </li>
              @endif
              @else
              <li>
                  <a class="nav-link fa fa-power-off" href="{{ route('logout') }}" style="color:white;" onclick="event.preventDefault(); 
                        document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </li>
          </ul>
          @endguest
    </div>
  </nav>
</header>

<main role="main">   
    <div class="container mt-5" style="max-width:98%;">
        <div class="row">
            <!-- Buttons -->
            @yield('buttons')
        </div>
    </div>
    <div class="container mt-1" style="max-width:98%;">
        <div class="row">
            <!-- Content -->
            @yield('content')
        </div>
    </div>
    
</main>

<!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2020 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
  </footer>

    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
 


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>



  </body>
</html>