
<link href="{{ asset('css/player_style.css') }}" rel="stylesheet" type="text/css" >
<!-- Load c3.css -->
<link href="{{ asset('css/c3.css') }}" rel="stylesheet" type="text/css" >

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<script>

  $(function(){
       var lastScrollTop = 0, delta = 5, currpos=0;
           $(window).scroll(function(){
             var nowScrollTop = $(this).scrollTop();
             if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
               var offset = $(".example").offset(); var w = $(window);
              if (nowScrollTop > lastScrollTop){
                      console.log('down');
                        currpos += 102 ;
                        console.log(document.body.scrollTop+'....'+document.documentElement.scrollTop);
                        //console.log(currpos);
                         
                        
                        // alert("(x,y): ("+(offset.left-w.scrollLeft())+","+(offset.top-w.scrollTop())+")");
                        //console.log(offset.left-w.scrollLeft()+","+(offset.top-w.scrollTop()));
                        //console.log(((offset.top-w.scrollTop())/10)+'px');
                        $('.example').css('top', document.body.scrollTop+'px' );
              } else {
                    console.log('up');
                    currpos = currpos - 102 ;
                    console.log(document.body.scrollTop+'....'+document.documentElement.scrollTop);
                    //console.log(currpos);
                   // console.log(  offset.left-w.scrollLeft()+","+(offset.top-w.scrollTop())  );
                    $('.example').css('top',document.body.scrollTop+'px');
              }
             lastScrollTop = nowScrollTop;
             }
       });
 });


  


  </script>

  
@extends('layouts.app')

@section('buttons')
<a class="btn btn-warning" href="/">Go Back</a>
@endsection

@section('content')

 <div id="chart"></div>
 

<style type="text/css">
        .c3-chart-text .c3-text {
            font-family: 'FontAwesome';
            font-weight: bolder;
            }

</style>


<div class="bs-example" style="width:100%;">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#upline" class="nav-link active" data-toggle="tab">Up Line</a>
        </li>
        <li class="nav-item">
            <a href="#downline" class="nav-link" data-toggle="tab">Down Line</a>
        </li>
        <li class="nav-item">
            <a href="#enquiry" class="nav-link" data-toggle="tab">Enquiry</a>
        </li>
        <li class="nav-item">
            <a href="#pnrstatus" class="nav-link" data-toggle="tab">PNR Status</a>
        </li>
        <li class="nav-item">
            <a href="#chkroute" class="nav-link" data-toggle="tab">Check Route</a>
        </li>
    </ul>
    <div class="tab-content" style="width:70%;float: left;">
        <div class="tab-pane fade show active" id="upline" style="padding-top: 20px;">                        
            <h4>Train Toward Shornur</h4>             

            <div id="home" class="shadow-lg p-3 mb-5 bg-white rounded border border-primary rounded">
                 <table id="tablefirst" class="table table-bordered table-hover" cellspacing="0" style="width:100%">
                        <thead>
                           <tr>
                                <th>Train #</th>
                                <th>Train Name</th>
                                <th>Schedule Arrival</th>
                                <th>Delay In Arrival</th>
                                <th>Start Date</th>	                                            
                        	</tr>
                        </thead>

                    	<tbody id="tbodyUp">

                    	</tbody>

    			</table>
            </div>
        </div>

        <div class="tab-pane fade" id="downline"  style="padding-top: 20px;">                            
            <h4>Train Toward Porur</h4>  
                    <div id="chart"></div>                          
            <div class="shadow-lg p-3 mb-5 bg-white rounded border border-primary rounded-lg">
               <table  id="tablesecond"  class="table table-bordered table-hover" cellspacing="0" style="width:100%">
                    <thead>
                         <tr>
                            <th>Train # </th>
                            <th>Train Name</th>
                            <th>Schedule Arrival</th>
                            <th>Delay In Arrival</th>
                            <th>Start Date</th>
                             
                        </tr>			                            
                    </thead>
                    <tbody id="tbodydown">
                    </tbody>
    			</table>
            </div>
        </div>

        <div class="tab-pane fade" id="enquiry"  style="padding-top: 20px;">
            <h4 class="mt-2">General Enquiry tab content</h4>
            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
        </div>

        <div class="tab-pane fade" id="pnrstatus"  style="padding-top: 20px;">
            <h4 class="mt-2">PNR STATUS tab content</h4>
            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
        </div>

        <div class="tab-pane fade" id="chkroute"  style="padding-top: 20px;">
            <h4 class="mt-2">Check Train Route tab content</h4>
            <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
        </div>
    </div>
</div>

<div class="example" style="position: fixed;top:100;right: 10;">
    <div class="player">
        <div class="pl"></div>
        <div class="title"></div>
        <div class="artist"></div>
        <div class="cover"></div>
        <div class="controls">
            <div class="play"></div>
            <div class="pause"></div>
            <div class="rew"></div>
            <div class="fwd"></div>
        </div>
        <<div class="volume"></div>
        <div class="tracker"></div>
    </div>
    <ul class="playlist1 hidden">
        <li audiourl="01.mp3" cover="mbr-192x128.jpg" artist="Artist 1">01.mp3</li>
        <li audiourl="02.mp3" cover="mbr-192x128.jpg" artist="Artist 2">02.mp3</li>
        <li audiourl="03.mp3" cover="mbr-192x128.jpg" artist="Artist 3">03.mp3</li>
        <li audiourl="04.mp3" cover="mbr-192x128.jpg" artist="Artist 4">04.mp3</li>
        <li audiourl="05.mp3" cover="mbr-192x128.jpg" artist="Artist 5">05.mp3</li>
        <li audiourl="06.mp3" cover="mbr-192x128.jpg" artist="Artist 6">06.mp3</li>
        <li audiourl="07.mp3" cover="mbr-192x128.jpg" artist="Artist 7">07.mp3</li>
    </ul>

    <ul class="playlist">
         <!-- <li audiourl="Train Number 56651 CBE-CAN PASSENGER is schedule to arrive at 20:01, 20 Nov" cover="mbr-192x128.jpg" artist="Artist 1">01.mp3</li> -->
    </ul>
</div>

 

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="{{URL::asset('js/jquery-ui-1.8.21.custom.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/player_script.js')}}"></script>
<!-- Load d3 & c3 js -->
<script type="text/javascript" src="{{ URL::asset('js/d3-5.8.2.min.js') }} " charset="utf-8"></script>
<script type="text/javascript" src="{{ URL::asset('js/c3.min.js') }}" charset="utf-8"></script>

<!-- chart script -->
<script type="text/javascript">
    var chart = c3.generate({
        data: {
            columns: [
                ['data1', 0,0,0]
                
            ],
                labels: {
                // format: function (v, id, i, j) { return "Default Format"; },
                format: {
                        data1: function (v, id, i, j) { return "\uf238"; },
                        //data1: function (v, id, i, j) { return "\uf238"; },
                     
                    }
            }
        },
        axis: {
          y: {
           show:false
            },
            x: {
           show:false
            }
          },
    });
     

</script>


<!-- Start - Audio Player  -->
<script type="text/javascript">
    $(document).ready(function() {
          var synthesis = window.speechSynthesis;
           if ('speechSynthesis' in window) {
                var synthesis = window.speechSynthesis; console.log('Text-to-speech  supported.');

            }
            else {
                console.log('Text-to-speech not supported.');
            }

            var utterance1 = new SpeechSynthesisUtterance("Train Number 1 6 3 3 6 GANDHIDHAM EXP is running late by 37 minutes and  is scheduled to arrive at 13 50");
            // var utterance2 = new SpeechSynthesisUtterance("Train Number 1 2 6 8 6 MAQ-MAS EXP is running late by 20 minutes and  is scheduled to arrive at 20 02");
            // var utterance3 = new SpeechSynthesisUtterance("Train Number 1 2 4 3 2 NZM-TVC RAJDHANI EXP is running ON TIME and  is scheduled to arrive at 21 02");
            //speechSynthesis.speak(utterance1);
            //speechSynthesis.speak(utterance2);
            // speechSynthesis.speak(utterance3);

    	})
</script>


<script>

	var data_api = [
        { trainNo: "56651", startDate: "20 Nov 2019", trainName: "CBE-CAN PASSENGER", trnName: function () { return _LANG === "en-us" ? "CBE-CAN PASSENGER" : "कोयंबटूर जं.कण्णुर पैसेन्जर" }, trainSrc: "CBE", trainDstn: "CAN", runsOn: "null", actArr: "20:01, 20 Nov", delayArr: "01:54", actDep: "20:02, 20 Nov", delayDep: "01:52", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "12686", startDate: "20 Nov 2019", trainName: "MAQ-MAS EXP.", trnName: function () { return _LANG === "en-us" ? "MAQ-MAS EXP." : "चेन्नईएक्स" }, trainSrc: "MAQ", trainDstn: "MAS", runsOn: "null", actArr: "20:02, 20 Nov", delayArr: "00:20", actDep: "20:03, 20 Nov", delayDep: "00:18", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12432", startDate: "19 Nov 2019", trainName: "NZM-TVC RAJDHANI EXP", trnName: function () { return _LANG === "en-us" ? "NZM-TVC RAJDHANI EXP" : "तिरुवनंतपुरम राजधानी एक्स्प्रेस" }, trainSrc: "NZM", trainDstn: "TVC", runsOn: "null", actArr: "21:02, 20 Nov", delayArr: "RIGHT TIME", actDep: "21:05, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "RAJ", pfNo: "4" },
        { trainNo: "16603", startDate: "20 Nov 2019", trainName: "MAVELI EXPRESS", trnName: function () { return _LANG === "en-us" ? "MAVELI EXPRESS" : "मावेली एक्स." }, trainSrc: "MAQ", trainDstn: "TVC", runsOn: "null", actArr: "21:58, 20 Nov", delayArr: "00:06", actDep: "22:01, 20 Nov", delayDep: "00:06", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56601", startDate: "20 Nov 2019", trainName: "SRR-CLT PASSENGER", trnName: function () { return _LANG === "en-us" ? "SRR-CLT PASSENGER" : "शोरानूर जं.कोज़िकोड पैसेन्जर" }, trainSrc: "SRR", trainDstn: "CLT", runsOn: "null", actArr: "20:25, 20 Nov", delayArr: "RIGHT TIME", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56652", startDate: "20 Nov 2019", trainName: "CAN-CLT PASSENGER", trnName: function () { return _LANG === "en-us" ? "CAN-CLT PASSENGER" : "कण्णुर कालीकट पैसेन्जर" }, trainSrc: "CAN", trainDstn: "CLT", runsOn: "null", actArr: "21:35, 20 Nov", delayArr: "01:10", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56663", startDate: "20 Nov 2019", trainName: "TCR-CLT PASSR.", trnName: function () { return _LANG === "en-us" ? "TCR-CLT PASSR." : "त्रिचूर कालीकट पैसेन्जर" }, trainSrc: "TCR", trainDstn: "CLT", runsOn: "null", actArr: "21:50, 20 Nov", delayArr: "RIGHT TIME", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "2" },
        { trainNo: "16336", startDate: "19 Nov 2019", trainName: "GANDHIDHAM EXP", trnName: function () { return _LANG === "en-us" ? "GANDHIDHAM EXP" : "गांधीधाम एक्स." }, trainSrc: "NCJ", trainDstn: "GIMB", runsOn: "null", actArr: "01:09, 20 Nov", delayArr: "00:37", actDep: "01:12, 20 Nov", delayDep: "00:37", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "22638", startDate: "19 Nov 2019", trainName: "WEST COAST EXP", trnName: function () { return _LANG === "en-us" ? "WEST COAST EXP" : "मंगलोर- चेन्नई वेस्ट कोस्ट एक्स." }, trainSrc: "MAQ", trainDstn: "MAS", runsOn: "null", actArr: "01:52, 20 Nov", delayArr: "RIGHT TIME", actDep: "01:55, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12431", startDate: "19 Nov 2019", trainName: "TVC-NZM RAJDHANI EXP", trnName: function () { return _LANG === "en-us" ? "TVC-NZM RAJDHANI EXP" : "राजधानीएक्स. /FONT" }, trainSrc: "TVC", trainDstn: "NZM", runsOn: "null", actArr: "02:02, 20 Nov", delayArr: "RIGHT TIME", actDep: "02:05, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "RAJ", pfNo: "4" },
        { trainNo: "12283", startDate: "19 Nov 2019", trainName: "ERS-NZM DURONTO EXPRESS", trnName: function () { return _LANG === "en-us" ? "ERS-NZM DURONTO EXPRESS" : "एर्णाकुलम हजरत निजामुद्दीन दुरन्तो" }, trainSrc: "ERS", trainDstn: "NZM", runsOn: "null", actArr: "02:47, 20 Nov", delayArr: "RIGHT TIME", actDep: "02:50, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "DRNT", pfNo: "0" },
        { trainNo: "16604", startDate: "19 Nov 2019", trainName: "TVC-MAQ MAVELI EXPRESS", trnName: function () { return _LANG === "en-us" ? "TVC-MAQ MAVELI EXPRESS" : "मावेली एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "03:37, 20 Nov", delayArr: "RIGHT TIME", actDep: "03:40, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "12685", startDate: "19 Nov 2019", trainName: "MAS-MAQ EXP.", trnName: function () { return _LANG === "en-us" ? "MAS-MAQ EXP." : "मंगलोर एक्स." }, trainSrc: "MAS", trainDstn: "MAQ", runsOn: "null", actArr: "04:22, 20 Nov", delayArr: "RIGHT TIME", actDep: "04:25, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "22654", startDate: "18 Nov 2019", trainName: "NZM-TVC WKLY SF EXP", trnName: function () { return _LANG === "en-us" ? "NZM-TVC WKLY SF EXP" : "निजामुद्दीन तिरुवनंत सुपर् बीकली फ़ास्ट्" }, trainSrc: "NZM", trainDstn: "TVC", runsOn: "null", actArr: "04:35, 20 Nov", delayArr: "00:08", actDep: "04:38, 20 Nov", delayDep: "00:08", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12618", startDate: "18 Nov 2019", trainName: "MNGLA LKSDP EXP", trnName: function () { return _LANG === "en-us" ? "MNGLA LKSDP EXP" : "मंगला लक्षद्वीप एक्स्प्रेस" }, trainSrc: "NZM", trainDstn: "ERS", runsOn: "null", actArr: "04:37, 20 Nov", delayArr: "RIGHT TIME", actDep: "04:40, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "56600", startDate: "20 Nov 2019", trainName: "CLT -SRR PASSENGER", trnName: function () { return _LANG === "en-us" ? "CLT -SRR PASSENGER" : "कोज़िकोड शोरानूर जं.पैसेन्जर" }, trainSrc: "CLT", trainDstn: "SRR", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "05:00, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "16629", startDate: "19 Nov 2019", trainName: "MALABAR EXP", trnName: function () { return _LANG === "en-us" ? "MALABAR EXP" : "मालाबार एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "05:07, 20 Nov", delayArr: "RIGHT TIME", actDep: "05:10, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "17606", startDate: "19 Nov 2019", trainName: "KCG-MAQ EXP", trnName: function () { return _LANG === "en-us" ? "KCG-MAQ EXP" : "काचेगुडा मंगलोर एक्सप्रेस" }, trainSrc: "KCG", trainDstn: "MAQ", runsOn: "null", actArr: "05:47, 20 Nov", delayArr: "RIGHT TIME", actDep: "05:50, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "16347", startDate: "19 Nov 2019", trainName: "TVC-MAQ EXPRESS", trnName: function () { return _LANG === "en-us" ? "TVC-MAQ EXPRESS" : "मंगलोर एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "06:17, 20 Nov", delayArr: "RIGHT TIME", actDep: "06:20, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56653", startDate: "20 Nov 2019", trainName: "CLT-CAN PASSENGER", trnName: function () { return _LANG === "en-us" ? "CLT-CAN PASSENGER" : "कालीकट कण्णुर पैसेन्जर" }, trainSrc: "CLT", trainDstn: "CAN", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "06:45, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56664", startDate: "20 Nov 2019", trainName: "CLT-TCR PASSR.", trnName: function () { return _LANG === "en-us" ? "CLT-TCR PASSR." : "कालीकट त्रिचूर पैसेन्जर" }, trainSrc: "CLT", trainDstn: "TCR", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "07:20, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "2" },
        { trainNo: "12601", startDate: "19 Nov 2019", trainName: "MANGALORE MAIL", trnName: function () { return _LANG === "en-us" ? "MANGALORE MAIL" : "मंगलोरमेल" }, trainSrc: "MAS", trainDstn: "MAQ", runsOn: "null", actArr: "07:27, 20 Nov", delayArr: "RIGHT TIME", actDep: "07:30, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "16527", startDate: "19 Nov 2019", trainName: "BAND-CAN EXPRESS", trnName: function () { return _LANG === "en-us" ? "BAND-CAN EXPRESS" : "यशवंतपुर कण्णूर एक्स." }, trainSrc: "YPR", trainDstn: "CAN", runsOn: "null", actArr: "07:52, 20 Nov", delayArr: "RIGHT TIME", actDep: "07:55, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56650", startDate: "20 Nov 2019", trainName: "CAN-CBE PASSENGER", trnName: function () { return _LANG === "en-us" ? "CAN-CBE PASSENGER" : "कण्णुर कोयंबटूर जं.पैसेन्जर" }, trainSrc: "CAN", trainDstn: "CBE", runsOn: "null", actArr: "07:55, 20 Nov", delayArr: "RIGHT TIME", actDep: "08:00, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "16308", startDate: "20 Nov 2019", trainName: "CAN-ALLEPPEY EXP", trnName: function () { return _LANG === "en-us" ? "CAN-ALLEPPEY EXP" : "अलेप्पी एक्स." }, trainSrc: "CAN", trainDstn: "ALLP", runsOn: "null", actArr: "08:15, 20 Nov", delayArr: "01:48", actDep: "08:18, 20 Nov", delayDep: "01:48", actHalt: "null", trainType: "MEX", pfNo: "0" }
    ];


	  $('#tablefirst').DataTable({
                "aaSorting": [],
              columnDefs: [{
              orderable: false,
              targets: 3
              }]
    	});

	  $('#tablesecond').DataTable({
                "aaSorting": [],
              columnDefs: [{
              orderable: false,
              targets: 3
              }]
      	});

	//	$('.dataTables_length').addClass('bs-select');

	var dt1 = $('#tablefirst').DataTable();
	var dt2 = $('#tablesecond').DataTable();

    var audiomsg = [];

	if (data_api.length > 0) {
	        for (i = 0; i < 30; i++) {
	            dt1.row.add([
                        data_api[i].trainNo ,
                        data_api[i].trainName ,
                        (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5)),
                        data_api[i].delayArr,data_api[i].startDate
                        //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='PlaythisAudio(\""+ data_api[i].trainNo +"_"+ data_api[i].trainName +"\")' />"
                        //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='playAudioTest()' />"                   
	            
	            
	            ]).draw();

                if (i <= 5) {
                        var txttoPlay = "Train Number "+data_api[i].trainNo.toString().split('').join(' ') +" "+ data_api[i].trainName +" is schedule to arrive at "+data_api[i].actArr ;
                        console.log(txttoPlay);
                        // <li audiourl="01.mp3" cover="mbr-192x128.jpg" artist="Artist 1">01.mp3</li>
                    $('.playlist').append("<li audiourl='"+txttoPlay+"' cover='mbr-192x128.jpg' artist='"+data_api[i].trainNo+"'  id='li_"+i+"'>" + data_api[i].trainNo + "-"+ data_api[i].trainName + "</li > "); 

                    //audiomsg.push ( "Train Number , "+data_api[i].trainNo.toString().split('').join(' ') + " " + data_api[i].trainName + (data_api[i].delayArr=='RIGHT TIME' ? "is running on time" :" is running late by "+ data_api[i].delayArr)+" and scheduled to arrive at "+ (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5))  );
                }

                 // Second table for DownLine

                
                    dt2.row.add([
                                    data_api[i].trainNo ,
                                    data_api[i].trainName ,
                                    (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5)),
                                    data_api[i].delayArr,data_api[i].startDate
                                    //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='PlaythisAudio(\""+ data_api[i].trainNo +"_"+ data_api[i].trainName +"\")' />"
                    
                    ]).draw();
                
	            
	        }           

	    }


	function PlaythisAudio(trainInfo) {
        // console.log("this one");
        // console.log(trainInfo);
        //$('li').parent().prepend("<li>" + trainInfo + "</li>");
        stopAudioTest();
        playAudioTest();
    }

    function stopAudioTest() {
        console.log("stop click");
        speechSynthesis.cancel();
    }

    function playAudioTest(){
            console.log(audiomsg[0]);
         for (k = 0; k < audiomsg.length; k++) {
            speechSynthesis.speak(new SpeechSynthesisUtterance(audiomsg[k]));
             

        };
    }

</script>

@endsection