<style type="text/css">
  #container {
        width: 100%;
        /*border-top: 1px solid #979797*/
  }


  .overlay {
        width: 100%;
        min-width: 1000px;
        background: #000;
        display: none;
        position: absolute;
        left: 0;
        top: 127px;
        text-indent: -99999px;
        z-index: 151
  }

  .home .overlay {
        height: 442px!important
  }

  .cf:after {
        clear: both;
        content: ' ';
        display: block;
        font-size: 0;
        line-height: 0;
        visibility: hidden;
        width: 0;
        height: 0
  }


  #contentWrap,
  .slideNav {
        width: 1000px;
        position: relative;
        margin: 0 auto
  }



  .innerContent {
        min-height: 500px;
        height: auto!important
  }

  .innerContent h2 {
        padding-top: 15px
  }


  .sidebar {
        width: 199px;
        background: #c32026;
        margin: 2px 0 0 2px;*/
        padding: 0;
        position: relative
  }

  .sidebar.right {
        background: none;
        margin: 0 5px 20px 0;
        position: inherit;
        top: 0!important
  }



  .alignLeft {
        float: left!important
  }

  .alignRight {
        float: right!important
  }

  .displayNone {
        display: none!important
  }


  #menuLeft li {
        display: block;
        font-size: .80em;
        padding: 6px 0 7px
  }

  #menuLeft li a {
        display: block;
        color: #fff;
        line-height: 1.38em;
        text-align: right;
        background: url(../../../Images/sprite.png) -40px -1281px no-repeat;
        padding: 0 25px 0 10px
  }

  #menuLeft li a:hover {
        font-family: droid_sansregular, Arial;
        text-decoration: none
  }

  #menuLeft ul li a:hover,
  #menuLeft ul ul li a:hover {
        font-family: droid_sansregular, Arial
  }

  #menuLeft li.parent>a:hover,
  #menuLeft li.parent .current,
  #menuLeft li.parent a.active {
        color: #fff;
        text-decoration: underline;
        background-position: -40px -1309px;
        font-family: droid_sansregular, Arial
  }

  #menuLeft li.current a {
        background: none;
        text-decoration: underline;
        font-size: 1.2em;
        color: #af050f;
        padding: 6px 13px 7px
  }

  #menuLeft li.current a:hover {
        color: #111;
        background: none!important
  }

  #menuLeft li a.last:hover {
        background-position: -40px -1279px
  }

  #menuLeft li>ul {
        display: none;
        padding: 10px 0 0 10px
  }

  #menuLeft li>ul li {
        border: none;
        color: #6d6d6d;
        font-size: .9em;
        padding: 0 10px 1px 0
  }

  #menuLeft li>ul li a {
        color: #ffdfc3;
        background-position: -57px -1470px;
        padding: 0 14px 0 0;
        line-height: 1.75em;
  }

  #menuLeft li>ul li a:hover {
        color: #ffdfc3;
        text-decoration: underline;
        background-position: -57px -1470px;
  }

  #menuLeft li.parent>ul li a.active {
        color: #ffdfc3;
        background-position: -57px -1470px;
        text-decoration: underline;
        font-weight: 700
  }

  #menuLeft li.parent>ul li a.current {
        color: #ffdfc3;
        background-position: 173px -1439px;
        font-weight: 700
  }

  #menuLeft li>ul li>ul {
        padding: 6px 0 10px 10px
  }

  #menuLeft li>ul li>ul li {
        padding-right: 0
  }

  #menuLeft li.parent>ul li>ul li a.active {
        color: #fff;
        font-weight: 700;
        background-position: 163px -1469px;
        text-decoration: underline
  }


  #menuLeft.staticMenu li a.menuExt {
        background: url(../../../Images/ext-link-icon.gif) no-repeat 182px center;
  }

  #menuLeft.staticMenu li a.pdfLink {
        background: url(../../../Images/PDF_doc.gif) no-repeat 179px center;
  }

  #content p.first {
        font-size: .8em;
        color: #4a4a4a;
        padding-bottom: 12px
  }


  .column2 {
        width: 776px;
        min-height: 580px;
        height: auto!important;
        padding: 0 0 20px 20px
  }

  .column3 {
        width: 552px;
        min-height: 480px;
        height: auto!important;
        padding: 0 20px
  }

</style>

 
<link href="{{ asset('css/player_style.css') }}" rel="stylesheet" type="text/css" >


<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>


<script>

  $(function(){
       var lastScrollTop = 0, delta = 5, currpos=0;
           $(window).scroll(function(){
             var nowScrollTop = $(this).scrollTop();
             if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
               var offset = $(".sidebar").offset(); var w = $(window);
              if (nowScrollTop > lastScrollTop){
                       //console.log('down');
                        //currpos += 102 ;
                        //console.log(document.body.scrollTop+'....'+document.documentElement.scrollTop);
                         
                        // alert("(x,y): ("+(offset.left-w.scrollLeft())+","+(offset.top-w.scrollTop())+")");
                        //console.log(offset.left-w.scrollLeft()+","+(offset.top-w.scrollTop()));
                        //console.log(((offset.top-w.scrollTop())/10)+'px');
                        $('.sidebar').css('top', document.body.scrollTop+'px' );
              } else {
                    //console.log('up');
                   // currpos = currpos - 102 ;
                   // console.log(document.body.scrollTop+'....'+document.documentElement.scrollTop);
                    //console.log(currpos);
                   // console.log(  offset.left-w.scrollLeft()+","+(offset.top-w.scrollTop())  );
                    $('.sidebar').css('top',document.body.scrollTop+'px');
              }
             lastScrollTop = nowScrollTop;
             }
       });
 });
</script>


@extends('layouts.app')
 

@section('content')


<div id="container">
    <div class="overlay" style="height: 909px; display: none; opacity: 0.75;">Overlay</div>
        <div id="contentWrap" class="innerContent cf" style="width:100%">
            <div class="sidebar alignLeft  col-3">
                  @include('trains.player')
            </div>

            <div id="content" class="column2 alignLeft col-8">                  
                  <div class="bs-example" style="width:100%;">
                      <ul class="nav nav-tabs">
                          <li class="nav-item">
                              <a href="#upline" class="nav-link active" data-toggle="tab">Up Line</a>
                          </li>
                          <li class="nav-item">
                              <a href="#downline" class="nav-link" data-toggle="tab">Down Line</a>
                          </li>
                          <li class="nav-item">
                              <a href="#enquiry" class="nav-link" data-toggle="tab">Enquiry</a>
                          </li>
                          <li class="nav-item">
                              <a href="#pnrstatus" class="nav-link" data-toggle="tab">PNR Status</a>
                          </li>
                          <li class="nav-item">
                              <a href="#chkroute" class="nav-link" data-toggle="tab">Check Route</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                          <div class="tab-pane fade show active" id="upline" style="padding-top: 20px;">                        
                              <h4>Train Toward Shornur</h4>
                              <div id="home" class="shadow-lg p-3 mb-5 bg-white rounded border border-primary rounded">
                                   <table id="tablefirst" class="table table-bordered table-hover" cellspacing="0" style="width:100%">
                                          <thead>
                                             <tr>
                                                  <th>Train #</th>
                                                  <th>Train Name</th>
                                                  <th>Start Date</th> 
                                                  <th>Schedule Arrival</th>
                                                  <th>Delay</th>         
                                            </tr>
                                          </thead>

                                        <tbody id="tbodyUp">

                                        </tbody>

                                     </table>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="downline"  style="padding-top: 20px;">                            
                              <h4>Train Toward Porur</h4>                                                          
                              <div class="shadow-lg p-3 mb-5 bg-white rounded border border-primary rounded-lg">
                                 <table  id="tablesecond"  class="table table-bordered table-hover" cellspacing="0" style="width:100%">
                                      <thead>
                                           <tr>
                                              <th>Train # </th>
                                              <th>Train Name</th>
                                              <th>Start Date</th> 
                                              <th>Schedule Arrival</th>
                                              <th>Delay</th>                                               
                                          </tr>                                 
                                      </thead>
                                      <tbody id="tbodydown">
                                      </tbody>
                            </table>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="enquiry"  style="padding-top: 20px;">
                              <h4 class="mt-2">General Enquiry tab content</h4>
                              <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
                          </div>

                          <div class="tab-pane fade" id="pnrstatus"  style="padding-top: 20px;">
                              <h4 class="mt-2">PNR STATUS tab content</h4>
                              <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
                          </div>

                          <div class="tab-pane fade" id="chkroute"  style="padding-top: 20px;">
                              <h4 class="mt-2">Check Train Route tab content</h4>
                                  @include('trains.station')
                          </div>
                      </div>
           
                  </div>
            </div>
        </div>
</div>
 
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>


  





<!-- Start - Audio Player  -->
<script type="text/javascript">
    $(document).ready(function() {
          var synthesis = window.speechSynthesis;
           if ('speechSynthesis' in window) {
                var synthesis = window.speechSynthesis; console.log('Text-to-speech  supported.');

            }
            else {
                console.log('Text-to-speech not supported.');
            }

            var utterance1 = new SpeechSynthesisUtterance("Train Number 1 6 3 3 6 GANDHIDHAM EXP is running late by 37 minutes and  is scheduled to arrive at 13 50");
            // var utterance2 = new SpeechSynthesisUtterance("Train Number 1 2 6 8 6 MAQ-MAS EXP is running late by 20 minutes and  is scheduled to arrive at 20 02");
            // var utterance3 = new SpeechSynthesisUtterance("Train Number 1 2 4 3 2 NZM-TVC RAJDHANI EXP is running ON TIME and  is scheduled to arrive at 21 02");
            //speechSynthesis.speak(utterance1);
            //speechSynthesis.speak(utterance2);
            // speechSynthesis.speak(utterance3);

      })
</script>


<script>

  var data_api = [
        { trainNo: "56651", startDate: "20 Nov 2019", trainName: "CBE-CAN PASSENGER", trnName: function () { return _LANG === "en-us" ? "CBE-CAN PASSENGER" : "कोयंबटूर जं.कण्णुर पैसेन्जर" }, trainSrc: "CBE", trainDstn: "CAN", runsOn: "null", actArr: "20:01, 20 Nov", delayArr: "01:54", actDep: "20:02, 20 Nov", delayDep: "01:52", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "12686", startDate: "20 Nov 2019", trainName: "MAQ-MAS EXP.", trnName: function () { return _LANG === "en-us" ? "MAQ-MAS EXP." : "चेन्नईएक्स" }, trainSrc: "MAQ", trainDstn: "MAS", runsOn: "null", actArr: "20:02, 20 Nov", delayArr: "00:20", actDep: "20:03, 20 Nov", delayDep: "00:18", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12432", startDate: "19 Nov 2019", trainName: "NZM-TVC RAJDHANI EXP", trnName: function () { return _LANG === "en-us" ? "NZM-TVC RAJDHANI EXP" : "तिरुवनंतपुरम राजधानी एक्स्प्रेस" }, trainSrc: "NZM", trainDstn: "TVC", runsOn: "null", actArr: "21:02, 20 Nov", delayArr: "RIGHT TIME", actDep: "21:05, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "RAJ", pfNo: "4" },
        { trainNo: "16603", startDate: "20 Nov 2019", trainName: "MAVELI EXPRESS", trnName: function () { return _LANG === "en-us" ? "MAVELI EXPRESS" : "मावेली एक्स." }, trainSrc: "MAQ", trainDstn: "TVC", runsOn: "null", actArr: "21:58, 20 Nov", delayArr: "00:06", actDep: "22:01, 20 Nov", delayDep: "00:06", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56601", startDate: "20 Nov 2019", trainName: "SRR-CLT PASSENGER", trnName: function () { return _LANG === "en-us" ? "SRR-CLT PASSENGER" : "शोरानूर जं.कोज़िकोड पैसेन्जर" }, trainSrc: "SRR", trainDstn: "CLT", runsOn: "null", actArr: "20:25, 20 Nov", delayArr: "RIGHT TIME", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56652", startDate: "20 Nov 2019", trainName: "CAN-CLT PASSENGER", trnName: function () { return _LANG === "en-us" ? "CAN-CLT PASSENGER" : "कण्णुर कालीकट पैसेन्जर" }, trainSrc: "CAN", trainDstn: "CLT", runsOn: "null", actArr: "21:35, 20 Nov", delayArr: "01:10", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56663", startDate: "20 Nov 2019", trainName: "TCR-CLT PASSR.", trnName: function () { return _LANG === "en-us" ? "TCR-CLT PASSR." : "त्रिचूर कालीकट पैसेन्जर" }, trainSrc: "TCR", trainDstn: "CLT", runsOn: "null", actArr: "21:50, 20 Nov", delayArr: "RIGHT TIME", actDep: "DESTINATION", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "2" },
        { trainNo: "16336", startDate: "19 Nov 2019", trainName: "GANDHIDHAM EXP", trnName: function () { return _LANG === "en-us" ? "GANDHIDHAM EXP" : "गांधीधाम एक्स." }, trainSrc: "NCJ", trainDstn: "GIMB", runsOn: "null", actArr: "01:09, 20 Nov", delayArr: "00:37", actDep: "01:12, 20 Nov", delayDep: "00:37", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "22638", startDate: "19 Nov 2019", trainName: "WEST COAST EXP", trnName: function () { return _LANG === "en-us" ? "WEST COAST EXP" : "मंगलोर- चेन्नई वेस्ट कोस्ट एक्स." }, trainSrc: "MAQ", trainDstn: "MAS", runsOn: "null", actArr: "01:52, 20 Nov", delayArr: "RIGHT TIME", actDep: "01:55, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12431", startDate: "19 Nov 2019", trainName: "TVC-NZM RAJDHANI EXP", trnName: function () { return _LANG === "en-us" ? "TVC-NZM RAJDHANI EXP" : "राजधानीएक्स. /FONT" }, trainSrc: "TVC", trainDstn: "NZM", runsOn: "null", actArr: "02:02, 20 Nov", delayArr: "RIGHT TIME", actDep: "02:05, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "RAJ", pfNo: "4" },
        { trainNo: "12283", startDate: "19 Nov 2019", trainName: "ERS-NZM DURONTO EXPRESS", trnName: function () { return _LANG === "en-us" ? "ERS-NZM DURONTO EXPRESS" : "एर्णाकुलम हजरत निजामुद्दीन दुरन्तो" }, trainSrc: "ERS", trainDstn: "NZM", runsOn: "null", actArr: "02:47, 20 Nov", delayArr: "RIGHT TIME", actDep: "02:50, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "DRNT", pfNo: "0" },
        { trainNo: "16604", startDate: "19 Nov 2019", trainName: "TVC-MAQ MAVELI EXPRESS", trnName: function () { return _LANG === "en-us" ? "TVC-MAQ MAVELI EXPRESS" : "मावेली एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "03:37, 20 Nov", delayArr: "RIGHT TIME", actDep: "03:40, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "12685", startDate: "19 Nov 2019", trainName: "MAS-MAQ EXP.", trnName: function () { return _LANG === "en-us" ? "MAS-MAQ EXP." : "मंगलोर एक्स." }, trainSrc: "MAS", trainDstn: "MAQ", runsOn: "null", actArr: "04:22, 20 Nov", delayArr: "RIGHT TIME", actDep: "04:25, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "22654", startDate: "18 Nov 2019", trainName: "NZM-TVC WKLY SF EXP", trnName: function () { return _LANG === "en-us" ? "NZM-TVC WKLY SF EXP" : "निजामुद्दीन तिरुवनंत सुपर् बीकली फ़ास्ट्" }, trainSrc: "NZM", trainDstn: "TVC", runsOn: "null", actArr: "04:35, 20 Nov", delayArr: "00:08", actDep: "04:38, 20 Nov", delayDep: "00:08", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "12618", startDate: "18 Nov 2019", trainName: "MNGLA LKSDP EXP", trnName: function () { return _LANG === "en-us" ? "MNGLA LKSDP EXP" : "मंगला लक्षद्वीप एक्स्प्रेस" }, trainSrc: "NZM", trainDstn: "ERS", runsOn: "null", actArr: "04:37, 20 Nov", delayArr: "RIGHT TIME", actDep: "04:40, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "56600", startDate: "20 Nov 2019", trainName: "CLT -SRR PASSENGER", trnName: function () { return _LANG === "en-us" ? "CLT -SRR PASSENGER" : "कोज़िकोड शोरानूर जं.पैसेन्जर" }, trainSrc: "CLT", trainDstn: "SRR", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "05:00, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "16629", startDate: "19 Nov 2019", trainName: "MALABAR EXP", trnName: function () { return _LANG === "en-us" ? "MALABAR EXP" : "मालाबार एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "05:07, 20 Nov", delayArr: "RIGHT TIME", actDep: "05:10, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "17606", startDate: "19 Nov 2019", trainName: "KCG-MAQ EXP", trnName: function () { return _LANG === "en-us" ? "KCG-MAQ EXP" : "काचेगुडा मंगलोर एक्सप्रेस" }, trainSrc: "KCG", trainDstn: "MAQ", runsOn: "null", actArr: "05:47, 20 Nov", delayArr: "RIGHT TIME", actDep: "05:50, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "16347", startDate: "19 Nov 2019", trainName: "TVC-MAQ EXPRESS", trnName: function () { return _LANG === "en-us" ? "TVC-MAQ EXPRESS" : "मंगलोर एक्स." }, trainSrc: "TVC", trainDstn: "MAQ", runsOn: "null", actArr: "06:17, 20 Nov", delayArr: "RIGHT TIME", actDep: "06:20, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56653", startDate: "20 Nov 2019", trainName: "CLT-CAN PASSENGER", trnName: function () { return _LANG === "en-us" ? "CLT-CAN PASSENGER" : "कालीकट कण्णुर पैसेन्जर" }, trainSrc: "CLT", trainDstn: "CAN", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "06:45, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "56664", startDate: "20 Nov 2019", trainName: "CLT-TCR PASSR.", trnName: function () { return _LANG === "en-us" ? "CLT-TCR PASSR." : "कालीकट त्रिचूर पैसेन्जर" }, trainSrc: "CLT", trainDstn: "TCR", runsOn: "null", actArr: "SOURCE", delayArr: "RIGHT TIME", actDep: "07:20, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "2" },
        { trainNo: "12601", startDate: "19 Nov 2019", trainName: "MANGALORE MAIL", trnName: function () { return _LANG === "en-us" ? "MANGALORE MAIL" : "मंगलोरमेल" }, trainSrc: "MAS", trainDstn: "MAQ", runsOn: "null", actArr: "07:27, 20 Nov", delayArr: "RIGHT TIME", actDep: "07:30, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "SUF", pfNo: "0" },
        { trainNo: "16527", startDate: "19 Nov 2019", trainName: "BAND-CAN EXPRESS", trnName: function () { return _LANG === "en-us" ? "BAND-CAN EXPRESS" : "यशवंतपुर कण्णूर एक्स." }, trainSrc: "YPR", trainDstn: "CAN", runsOn: "null", actArr: "07:52, 20 Nov", delayArr: "RIGHT TIME", actDep: "07:55, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "MEX", pfNo: "0" },
        { trainNo: "56650", startDate: "20 Nov 2019", trainName: "CAN-CBE PASSENGER", trnName: function () { return _LANG === "en-us" ? "CAN-CBE PASSENGER" : "कण्णुर कोयंबटूर जं.पैसेन्जर" }, trainSrc: "CAN", trainDstn: "CBE", runsOn: "null", actArr: "07:55, 20 Nov", delayArr: "RIGHT TIME", actDep: "08:00, 20 Nov", delayDep: "RIGHT TIME", actHalt: "null", trainType: "PAS", pfNo: "0" },
        { trainNo: "16308", startDate: "20 Nov 2019", trainName: "CAN-ALLEPPEY EXP", trnName: function () { return _LANG === "en-us" ? "CAN-ALLEPPEY EXP" : "अलेप्पी एक्स." }, trainSrc: "CAN", trainDstn: "ALLP", runsOn: "null", actArr: "08:15, 20 Nov", delayArr: "01:48", actDep: "08:18, 20 Nov", delayDep: "01:48", actHalt: "null", trainType: "MEX", pfNo: "0" }
    ];


    $('#tablefirst').DataTable({
                "aaSorting": [],
              columnDefs: [{
              orderable: false,
              targets: 3
              }]
      });

    $('#tablesecond').DataTable({
                "aaSorting": [],
              columnDefs: [{
              orderable: false,
              targets: 3
              }]
        });

  //  $('.dataTables_length').addClass('bs-select');

  var dt1 = $('#tablefirst').DataTable();
  var dt2 = $('#tablesecond').DataTable();

    var audiomsg = [];

  if (data_api.length > 0) {
          for (i = 0; i < data_api.length; i++) {
              dt1.row.add([
                        data_api[i].trainNo ,
                        data_api[i].trainName ,data_api[i].startDate ,
                        (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5)),
                        data_api[i].delayArr
                        //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='PlaythisAudio(\""+ data_api[i].trainNo +"_"+ data_api[i].trainName +"\")' />"
                        //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='playAudioTest()' />"                   
              
              
              ]).draw();

                if (i <= 5) {
                        var txttoPlay = "Train Number "+data_api[i].trainNo.toString().split('').join(' ') +" "+ data_api[i].trainName +" is schedule to arrive at "+data_api[i].actArr ;
                        console.log(txttoPlay);
                        // <li audiourl="01.mp3" cover="mbr-192x128.jpg" artist="Artist 1">01.mp3</li>
                    $('.playlist').append("<li audiourl='"+txttoPlay+"' cover='mbr-192x128.jpg' artist='"+data_api[i].trainNo+"'  id='li_"+i+"'>" + data_api[i].trainNo + "-"+ data_api[i].trainName + "</li > "); 

                    //audiomsg.push ( "Train Number , "+data_api[i].trainNo.toString().split('').join(' ') + " " + data_api[i].trainName + (data_api[i].delayArr=='RIGHT TIME' ? "is running on time" :" is running late by "+ data_api[i].delayArr)+" and scheduled to arrive at "+ (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5))  );
                }

                 // Second table for DownLine

                
              dt2.row.add([
                              data_api[i].trainNo ,
                              data_api[i].trainName ,data_api[i].startDate,
                              (data_api[i].actArr.search(',') == -1 ? data_api[i].actArr : data_api[i].actArr.substring(0, 5)),
                              data_api[i].delayArr,data_api[i].startDate
                              //"<input type='button' value='Play Audio' class='btn-sm btn-info' onclick='PlaythisAudio(\""+ data_api[i].trainNo +"_"+ data_api[i].trainName +"\")' />"
                    
                    ]).draw();               
              
          }           

      }


  function PlaythisAudio(trainInfo) {
        // console.log("this one");
        // console.log(trainInfo);
        //$('li').parent().prepend("<li>" + trainInfo + "</li>");
        stopAudioTest();
        playAudioTest();
    }

    function stopAudioTest() {
        console.log("stop click");
        speechSynthesis.cancel();
    }

    function playAudioTest(){
            console.log(audiomsg[0]);
         for (k = 0; k < audiomsg.length; k++) {
            speechSynthesis.speak(new SpeechSynthesisUtterance(audiomsg[k]));
             

        };
    }

</script>



@endsection


