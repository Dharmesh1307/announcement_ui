
<style type="text/css">
        .c3-chart-text .c3-text {
            font-family: 'FontAwesome';
            font-weight: bolder;
            }

</style>

<!-- Load c3.css -->
<link href="{{ asset('css/c3.css') }}" rel="stylesheet" type="text/css" >

<h2 class="featurette-heading" style="margin-top: 2rem;color:red;">Rail Network - Nearby Stations.</span></h2>
<div id="chart" style="background-color: wheat;border-radius: 15px;"></div> 



<script type="text/javascript" src="{{URL::asset('js/jquery-ui-1.8.21.custom.min.js')}}"></script>
<!-- Load d3 & c3 js -->
<script type="text/javascript" src="{{ URL::asset('js/d3-5.8.2.min.js') }} " charset="utf-8"></script>
<script type="text/javascript" src="{{ URL::asset('js/c3.min.js') }}" charset="utf-8"></script>


<!-- chart script -->
<script type="text/javascript">
    var chart = c3.generate({
        data: {
               
            columns: [

                ['name', 0,0,0]
                
            ],
                labels: {
                // format: function (v, id, i, j) { return "Default Format"; },
                format: {
                        name: function (v, id, i, j) { return "\uf192"; },
                        //data1: function (v, id, i, j) { return "\uf238"; },
                     
                    }
            }
        },
        legend: {
              show: false
        },
        axis: {
            y: {
                  show:false
              },
              x: {
                  show:false
              }
          },

          tooltip: {
       				 format: {
			            title: function (d) {  return "Station Name"; } // return 'Data ' + d; }
			//            value: d3.format(',') // apply this format to both y and y2
			        }
      			},


    });
     

</script>


