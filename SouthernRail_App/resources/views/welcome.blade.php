
 
@extends('layouts.app')

@section('content')
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
         <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"> 
            
            <!--<rect width="100%" height="100%" fill="#777"/>-->
            <img src="{{ asset('images/mbr-1920x1211.jpg') }}" alt="">
         </svg>
         
        <div class="container">
          <div class="carousel-caption text-left">
            <h1>Indian Railways - Southern Division</h1>            
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <!--<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>-->
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
            <img src="{{ asset('images/mbr-1920x1211.jpg') }}" alt="">
        </svg>
        <div class="container">
          <div class="carousel-caption">
            <h1>Another example headline.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
            <img src="{{ asset('images/mbr-1920x1211.jpg') }}" alt="">
        </svg>
        <div class="container">
          <div class="carousel-caption text-right">
            <h1>One more for good measure.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>



  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
     <hr class="featurette-divider">
     
    <div class="row">
       
      <div class="col-lg-4">
         
           <img src="{{ asset('images/covid_coach.jpg') }}" alt="">       
        
        <p>Coronavirus COVID-19: Southern railways complete conversion of 573 coaches into isolation wards.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      
      <div class="col-lg-4">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text></svg>
        <h2>Heading</h2>
        <p>DDD rakes on train to be replaced with AC coaches for 2 days.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      
      <div class="col-lg-4">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text></svg>
        <h2>Heading</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->
    <hr class="featurette-divider">
    
    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading" style="margin-top: 2rem;">Rail Network.<span class="text-muted">Nearby Station.</span></h2>       
        <p class="lead">Holistic view of Southern Railways nearest stations using open source JS library.</p>
        <p><a class="btn btn-lg btn-warning" href="{{ route('trains.index') }}"  role="button">Learn More</a></p>
      </div>
       <div class="col-md-5">
        <img src="{{ asset('images/stations.jpg') }}" alt=""> 
      </div>

    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading" style="margin-top: 2rem;color:red;">Interactive Audio Player <span class="text-muted">See for yourself.</span></h2>
        <p class="lead">Train Announcement Audio player uses SpeechSynthesisUtterance service to play in English...</p>
        <p><a class="btn btn-lg btn-primary" href="{{ route('trains.index') }}"  role="button">Check Out</a></p>
      </div>
      <div class="col-md-5 order-md-1">
        <img src="{{ asset('images/player_snap.jpg') }}" alt="">        
      </div>
    </div>

   <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading" style="margin-top: 2rem;color:red;">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
        <p class="lead">Close to real time train status and arrival details.No need to manaully refresh any pages or any button click. This will help our operators extra mile as this comes with Train number / Name search. </p>
        <p><a class="btn btn-lg btn-success" href="{{ route('trains.index') }}"  role="button">Go Explore</a></p>
      </div>
      <div class="col-md-5">
        <img src="{{ asset('images/trainList.jpg') }}" alt="">
      </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

  </div><!-- /.container -->


 
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>



@endsection